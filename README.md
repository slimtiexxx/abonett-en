# Abonett Frontend Demo
Simple AngularJS project with SASS support and Gulp watch/build tasks
### [Preview the website HERE](http://andrewsite.webuda.com/abonett-en/)

# Features

## 1. Download
```bash
git clone https://gitlab.com/slimtiexxx/abonett-en.git
```

## 2. Setup
```bash
npm install
```

- all SCSS/HTML will be watched for changes and injected into browser thanks to BrowserSync

## 3. Build project js
```bash
gulp scripts
```
## 4. Build project css
```bash
gulp sass
```

## Or all in together run building process
```bash
gulp serve
```

- this will process following tasks:
* start a webserver using Browserify live reload
* compile SASS files, minify and uncss compiled css
* minify and copy all JS files