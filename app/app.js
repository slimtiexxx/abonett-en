var angular = require('angular');

// Import Config
var config = require('./config');
var MenuHandler = require('./menu-handler');

// Import Controllers
var ProductsController = require('./product-controller');

// Import Directives
var footerDirective = require('../components/footer.directive.js');
var headerDirective = require('../components/header.directive.js');

// Import Dependencies
require('angular-route');


angular
    .module('abonett', [
        'ngRoute'
    ])

    // Menu handler
    .run(MenuHandler)

    // Config inject
    .config(config)

    // Controllers inject
    .controller('ProductsController', ProductsController)

    // Directives inject
    .directive('footerDirective', footerDirective)
    .directive('headerDirective', headerDirective);
