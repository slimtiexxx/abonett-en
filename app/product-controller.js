function productController($scope, $http, $rootScope, $location){
    $http.get('/data/products.json').then(function(response) {
        var data = response.data;

        $scope.welcome = data.welcome;
        $scope.products = data.products;
    });
}

productController.$inject = ['$scope', '$http', '$rootScope', '$location'];

module.exports = productController;