
function config($routeProvider, $locationProvider) {

    // routes
    $routeProvider
        .when('/', {
            templateUrl: 'views/product.html'
            /* The template will be home.html */
        })
        .when('/home', {
            templateUrl: 'views/home.html'
        })
        .when('/about-us', {
            templateUrl: 'views/about-us.html'
        })
        .when('/products', {
            templateUrl: 'views/product.html'
        })
        .when('/trade', {
            templateUrl: 'views/trade.html'
        })
        .when('/contact', {
            templateUrl: 'views/contact.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);

}

// Config Injections
config.$inject = ['$routeProvider', '$locationProvider'];

module.exports = config;