
function menuHandler($rootScope, $location){

    $rootScope.menus = [
        {
            "name" : "Home",
            "url" : "/home"
        },
        {
            "name" : "About Us",
            "url" : "/about-us"
        },
        {
            "name" : "Products",
            "url" : "/products"
        },
        {
            "name" : "Trade",
            "url" : "/trade"
        },
        {
            "name" : "Contact",
            "url" : "/contact"
        }
    ];

    $rootScope.setMenu = function (path) {
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
    };
}

menuHandler.$inject = ['$rootScope', '$location'];

module.exports = menuHandler;