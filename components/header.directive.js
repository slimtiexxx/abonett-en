function headerDirective() {

    // Definition of directive
    var directiveDefinitionObject = {
        restrict: 'E',
        templateUrl: '../components/header.html'
    };

    return directiveDefinitionObject;
}

module.exports = headerDirective;