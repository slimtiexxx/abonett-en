function footerDirective() {

    // Definition of directive
    var directiveDefinitionObject = {
        restrict: 'E',
        templateUrl: '../components/footer.html'
    };

    return directiveDefinitionObject;
}

module.exports = footerDirective;